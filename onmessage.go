package main

import (
	"log"
	"strings"
	"math/rand"
	"fmt"
	
	"github.com/bwmarrin/discordgo"
)

var (
	cmdPrefix = "!" // TODO: fill this in. This is the command prefix for your bot. Discord has added a slash command system. This template doesn't support that yet.
)

func onMessage(session *discordgo.Session, message *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if message.Author.ID == session.State.User.ID {
		return
	}
	
	defer func() {
		if p := recover(); p != nil {
			errorMessage := reportErr("Internal error in onMessage()", p)
			defer func() {
				if err := recover(); err != nil {
					log.Printf("Error in onMessage recovery: %v", p)
					shutdn<-true
				}
			}()
			session.ChannelMessageSend(message.ChannelID, errorMessage)
		}
	}()
	
	switch {
	case strings.HasPrefix(message.Content, cmdPrefix):
		spaceIndex := strings.IndexByte(message.Content[len(cmdPrefix):], ' ')
		
		var commandString, arg string
		if spaceIndex == -1 {
			commandString = message.Content[len(cmdPrefix):]
		} else {
			spaceIndex += len(cmdPrefix) // TODO NEXT: does this work? It was 2
			commandString = message.Content[len(cmdPrefix):spaceIndex]
			arg = message.Content[spaceIndex+1:]
		}
		
		command, ok := commands[commandString]
		if ok {
			load, _ := cmdData.Load(commandString)
			datum := load.(cmdDatum)
			if datum.disabled && !(isAdminMsg(message)) { // if the command is disabled, excepting admin(s)
				session.ChannelMessageSend(message.ChannelID, "Problem: "+commandString+" is disabled!")
			} else {
				go fmt.Println(message.Content)

				//load, _ = cmdData.Load(commandString)
				datum.runsSinceStart++
				cmdData.Store(commandString, datum)
				command.main(arg, session, message)
			}
			return
		} else if isAdminMsg(message) {
			command, ok := rootCommands[commandString]
			if ok {
				log.Printf("ROOT: %v %v\n", commandString, arg)
				command.main(arg, session, message)
				return
			}
		}
		session.ChannelMessageSend(message.ChannelID, "Problem: "+commandString+" is not a command!")
	}
}

func randMentionedText() string {
	text := [...]string{"Huh? ","Really? ","Aha. ","Do you really think so :pleading_face: "}
	return text[rand.Intn(len(text))] + text[rand.Intn(len(text))]
}
