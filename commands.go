package main

import (
	"github.com/bwmarrin/discordgo"
)

var commands map[string]*command

func init() {
	// Making the declaration and initialization separate this way prevents an initialization loop.
	// Also, there's another list in the "command data.go" file that must match this list of commands.
	commands = map[string]*command {
		"help": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				if arg != "" {
					command, ok := commands[arg]
					if ok {
						session.ChannelMessageSend(message.ChannelID, command.desc)
						return;
					}
				}
				session.ChannelMessageSend(
					message.ChannelID,
					`You can put a summary of your bot here, with command summaries.`, // TODO: fill this in
				)
			},
			desc: `Gives information about this bot and a command list.`, // TODO: fill this in
		},
		"ping": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				session.ChannelMessageSend(message.ChannelID, "Pong!")
			},
			desc: `Sends "Pong!". Use it to test `+programName+`'s speed, and whether it's working.`,
		},
	}
}
