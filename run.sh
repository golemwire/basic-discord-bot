echo "You should consider having this script sandbox your Discord bot... just in case."

while true ; do
	../basic-discord-bot && exit
	printf "EXIT CODE $?..."
	sleep 0.25 || exit
	echo "RESTARTING"
done
