package main

import (
	"sync"
)

type cmdDatum struct {
	disabled bool
	runsSinceStart int
}

var (
	cmdData = &sync.Map{}
)

func init() {
	cmdData.Store("help", cmdDatum{})
	cmdData.Store("ping", cmdDatum{})
}
