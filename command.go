package main

import (	
	"github.com/bwmarrin/discordgo"
)

type command struct {
	main func(arg string, session *discordgo.Session, message *discordgo.MessageCreate)
	// What the command does; a description from the 3rd-person view, with "it" conjugation (?)
	desc string
}
