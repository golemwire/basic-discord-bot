package main

import (
	"github.com/bwmarrin/discordgo"
)

func isAdminMsg(message *discordgo.MessageCreate) bool {
	// TODO: fill this in: Insert the user IDs of the bot admins here:
	var adminIDs = map[string]bool{
		"[YOUR USER ID]": true,
	}
	return adminIDs[message.Author.ID]
}
