package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/bwmarrin/discordgo"
)

var rootCommands = map[string]*command {
	// Toggles whether a command functions. Toggle states do not persist across restarts.
	"tog": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			
			if _, ok := commands[arg]; ok { // if the command exists:
				load, _ := cmdData.Load(arg)
				datum := load.(cmdDatum)
				datum.disabled = !datum.disabled
				cmdData.Store(arg, datum)
				// print what it is set to:
				if datum.disabled {
					session.ChannelMessageSend(message.ChannelID, "Command disabled!")
				} else {
					session.ChannelMessageSend(message.ChannelID, "Command enabled!")
				}
			} else {
				session.ChannelMessageSend(message.ChannelID, arg+" is not a command!")
			}
		},
	},
	"fail": &command{
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			session.ChannelMessageSend(message.ChannelID, reportErr("[No problem]", nil))
		},
	},
	"sys": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			switch arg {
			case "stop": // Shut down bot
				session.ChannelMessageSend(message.ChannelID, "Stopping...")
				shutdn<-false
			case "": // Show status
				// Print info about the bot's status.
				rcmdsStr := "Commands (name, runs-since-start):\n"
				cmdData.Range(func(key, value interface{}) bool {
					//rcmdsStr = append(rcmdsStr, key.(string) + " " + value.(cmdDatum).runsSinceStart)
					disabled := value.(cmdDatum).disabled
					if disabled {
						rcmdsStr += "[DISABLED] "
					}
					rcmdsStr += fmt.Sprintf("%s %v\n", key.(string), value.(cmdDatum).runsSinceStart)
					return true
				})
				session.ChannelMessageSend(message.ChannelID, rcmdsStr)
			case "restart": // Restart bot
				session.ChannelMessageSend(message.ChannelID, "Restarting...")
				shutdn<-true
			case "reset": // EMERGENCY STOP BOT. AVOID USING
				os.Exit(1)
			default:
				session.ChannelMessageSend(message.ChannelID, arg+" is not a sub-command!")
			}
		},
	},
	// Linux host commands.
	"host": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			switch arg {
			case "poweroff": // Shut down host
				session.ChannelMessageSend(message.ChannelID, "Shutting down host...")
				exec.Command("systemctl", "poweroff").Start()
				shutdn<-false
			case "reboot": // Reboot host
				session.ChannelMessageSend(message.ChannelID, "Rebooting host...")
				exec.Command("systemctl", "reboot").Start()
				shutdn<-false
			default:
				session.ChannelMessageSend(message.ChannelID, arg+" is not a sub-command!")
			}
		},
	},
	/*"del": &command{
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			err := session.ChannelMessageDelete(message.ChannelID, arg)
			if err != nil { reportErr("$ del failed", err) }
		//	// Delete this message ("$ del")
		//	err = session.ChannelMessageDelete(message.ChannelID, message.ID)
		//	if err != nil { reportErr("$ del failed", err) }
		},
	},*/
}
